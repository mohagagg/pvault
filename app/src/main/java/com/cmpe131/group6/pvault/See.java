package com.cmpe131.group6.pvault;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.drawable.Drawable;
import android.media.Image;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.ActionBar;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.os.Build;
import android.widget.ImageButton;
import android.widget.TextView;


public class See extends ActionBarActivity {
    ImageButton imageButton;
    public static final String DEFAULT = "N/A";
    TextView textView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_see);
        if (savedInstanceState == null) {
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.container, new PlaceholderFragment())
                    .commit();
        }

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
//        getMenuInflater().inflate(R.menu.menu_see, menu);
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_see, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    /**
     * A placeholder fragment containing a simple view.
     */
    public static class PlaceholderFragment extends Fragment {

        public PlaceholderFragment() {
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.fragment_see, container, false);
            return rootView;
        }
    }

    public void theButton(View v1)
    {
        imageButton = (ImageButton) findViewById(R.id.imageButtonSee);
        textView = (TextView) findViewById(R.id.textView5);
        SharedPreferences sharedPreferences = getSharedPreferences("MyData", Context.MODE_PRIVATE);
        String imageName = sharedPreferences.getString("Button", DEFAULT);
        String text = sharedPreferences.getString("CardName", DEFAULT);

            if ((imageName.equals("Visa")))
            {
                imageButton.setImageResource(R.drawable.visa_button);
                textView.setText(text);
            }
        else if (imageName.equals("MasterCard"))
            {
                imageButton.setImageResource(R.drawable.mastercard);
                textView.setText(text);
            }
        else if(imageName.equals("AmericanExpress"))
            {
                imageButton.setImageResource(R.drawable.american_express);
                textView.setText(text);
            }
        else if (imageName.equals("Discover"))
            {
                imageButton.setImageResource(R.drawable.discover_card);
                textView.setText(text);
            }
        else if ((imageName.equals("BOA")))
            {
                imageButton.setImageResource(R.drawable.bank_of_america_icon);
                textView.setText(text);
            }
        else if (imageName.equals("Chase"))
            {
                imageButton.setImageResource(R.drawable.chaselogo);
                textView.setText(text);
            }
        else if(imageName.equals("WellsFargo"))
            {
                imageButton.setImageResource(R.drawable.wellsfargo);
                textView.setText(text);
            }
        else if (imageName.equals("Citi"))
            {
                imageButton.setImageResource(R.drawable.citilogo);
                textView.setText(text);
            }
        else if (imageName.equals("Social"))
            {
                imageButton.setImageResource(R.drawable.social_security_card);
                textView.setText(text);
            }
        else if(imageName.equals("Passport"))
            {
                imageButton.setImageResource(R.drawable.passport_icon);
                textView.setText(text);
            }
        else if(imageName.equals("Certificate"))
            {
                imageButton.setImageResource(R.drawable.birthcertificate);
                textView.setText(text);
            }
    }
}