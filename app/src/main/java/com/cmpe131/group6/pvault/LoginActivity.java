package com.cmpe131.group6.pvault;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.ActionBarActivity;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Toast;

import java.util.LinkedList;


public class LoginActivity extends ActionBarActivity {

    EditText login;
    EditText pass;
    public static final String DEFAULT = "N/A";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);
        if (savedInstanceState == null) {
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.container, new PlaceholderFragment())
                    .commit();
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_login, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed()
    {

    }

    /**
     * A placeholder fragment containing a simple view.
     */
    public static class PlaceholderFragment extends Fragment {

        public PlaceholderFragment() {
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.fragment_login, container, false);
            return rootView;
        }
    }

    public void login(View v1)
    {
        login = (EditText) findViewById(R.id.editText1);
        pass = (EditText) findViewById(R.id.editText2);
        SharedPreferences sharedPreferences = getSharedPreferences("MyData", Context.MODE_PRIVATE);
        String tempLogin = sharedPreferences.getString("login", DEFAULT);
        String tempPass = sharedPreferences.getString("password", DEFAULT);

        if(tempLogin.equals(DEFAULT) || tempPass.equals(DEFAULT))
        {
            Toast.makeText(getApplicationContext(), "No accounts", Toast.LENGTH_SHORT).show();
        }
        else
        {
            if(tempLogin.equals(login) && tempPass.equals(pass))
            {
                Intent nextIntent = new Intent(getApplicationContext(), Files.class);
                startActivity(nextIntent);
            }
            else
            {
                Toast.makeText(getApplicationContext(), "Invalid Username or Password", Toast.LENGTH_SHORT).show();
            }
        }


//        if(loopSize == 0) {
//            Toast.makeText(getApplicationContext(), "No Accounts Made Yet", Toast.LENGTH_SHORT).show();
//        }
//        else {
//            int i = 0;
//            while(!found && i < loopSize) {
//                tempLogin = loginID.get(i);
//                if (tempLogin.equals(login)) {
//                    tempPass = password.get(i);
//                    if (tempPass.equals(pass)) {
//                        Intent nextIntent = new Intent(getApplicationContext(), Files.class);
//                        startActivity(nextIntent);
//                    } else {
//                        Toast.makeText(getApplicationContext(), "Invalid Password", Toast.LENGTH_LONG).show();
//                        found = true;
//                    }
//                } else {
//                    i++;
//                }
//            }
//            Toast.makeText(getApplicationContext(), "Invalid ID", Toast.LENGTH_LONG).show();
//        }

        /*
        nextIntent.putExtra("username", R.id.editText1.getText().toString());
        */
    }

    public void signup(View v1)
    {
        Intent nextIntent = new Intent(getApplicationContext(), signingup.class);
        startActivity(nextIntent);

        /*
        nextIntent.putExtra("username", editText1.getText().toString());
         */
    }
}
