package com.cmpe131.group6.pvault;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.ActionBar;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.os.Build;
import android.widget.EditText;
import android.widget.Toast;

import java.util.LinkedList;


public class signingup extends ActionBarActivity {
    EditText loginSU;
    EditText passSU;
    EditText passSU2;
    String login;
    String password;
    String passwordCon;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signingup);
        if (savedInstanceState == null) {
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.container, new PlaceholderFragment())
                    .commit();
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_signingup, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    /**
     * A placeholder fragment containing a simple view.
     */
    public static class PlaceholderFragment extends Fragment {

        public PlaceholderFragment() {
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.fragment_signingup, container, false);
            return rootView;
        }
    }

    public void finalsign(View v1){
        loginSU = (EditText) findViewById(R.id.editText1);
        login = loginSU.getText().toString();
        passSU = (EditText) findViewById(R.id.editText3);
        password = passSU.getText().toString();
        passSU2 = (EditText) findViewById(R.id.editText);
        passwordCon = passSU2.getText().toString();
        if(password.equals(null))
        {
            Toast.makeText(getApplicationContext(), "Enter a password", Toast.LENGTH_SHORT).show();
        }
        if(passwordCon.equals(null))
        {
            Toast.makeText(getApplicationContext(), "Retype password", Toast.LENGTH_SHORT).show();
        }
        if(password.equals(passwordCon) && password.length() > 0 && passwordCon.length() > 0)
            {
                SharedPreferences sharedPreferences = getSharedPreferences("MyData", Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = sharedPreferences.edit();
                editor.putString("login", login);
                editor.putString("password", password);
                editor.commit();

                Toast.makeText(getApplicationContext(), "Thanks for signing up", Toast.LENGTH_SHORT).show();
                Intent nextIntent = new Intent(getApplicationContext(), Files.class);
                startActivity(nextIntent);
            }
        else if(!password.equals(passwordCon))
        {
            Toast.makeText(getApplicationContext(), "Passwords do not match", Toast.LENGTH_SHORT).show();
        }
    }

}
