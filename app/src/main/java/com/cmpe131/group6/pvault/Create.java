package com.cmpe131.group6.pvault;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.ActionBar;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.os.Build;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.Toast;


public class Create extends ActionBarActivity {

    private Button button1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create);
        if (savedInstanceState == null) {
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.container, new PlaceholderFragment())
                    .commit();
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_create, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    /**
     * A placeholder fragment containing a simple view.
     */
    public static class PlaceholderFragment extends Fragment {

        public PlaceholderFragment() {
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            return inflater.inflate(R.layout.fragment_create, container, false);
        }
    }

    public void visa(View v1)
    {
        SharedPreferences sharedPreferences = getSharedPreferences("MyData", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        switch (v1.getId())
        {
            case R.id.imageButtonVisa:
                editor.putString("Button", "Visa");
                editor.commit();
                break;

            case R.id.imageButtonMC:
                editor.putString("Button", "MasterCard");
                editor.commit();
                break;

            case R.id.imageButtonAE:
                editor.putString("Button", "AmericanExpress");
                editor.commit();
                break;

            case R.id.imageButtonDC:
                editor.putString("Button", "Discover");
                editor.commit();
                break;

            case R.id.imageButtonBOA:
                editor.putString("Button", "BOA");
                editor.commit();
                break;

            case R.id.imageButtonCHS:
                editor.putString("Button", "Chase");
                editor.commit();
                break;

            case R.id.imageButtonWF:
                editor.putString("Button", "WellsFargo");
                editor.commit();
                break;

            case R.id.imageButtonCB:
                editor.putString("Button", "Citi");
                editor.commit();
                break;

            case R.id.imageButtonSS:
                editor.putString("Button", "Social");
                editor.commit();
                break;

            case R.id.imageButtonID:
                editor.putString("Button", "ID");
                editor.commit();
                break;

            case R.id.imageButtonPP:
                editor.putString("Button", "Passport");
                editor.commit();
                break;

            case R.id.imageButtonBC:
                editor.putString("Button", "Certificate");
                editor.commit();
                break;
        }
        Intent nextIntent = new Intent(getApplicationContext(), CreateCard.class);
        startActivity(nextIntent);
    }



}
